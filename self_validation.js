let DataValidator = require('./validator')
class Validator{
    lengthIsBetween(str, min, max){
        return str.length >= min &&
        str.length <= max
    }
    lessThanOrEqual(str, length){
        return str.length <= length
    }
    moreThanOrEqual(str, length){
        return str.length >= length
    }
    containsChar(str, ch){
        return str.indexOf(ch) != -1
    }
    doesntContainAchar(str, ch){
        return str.indexOf(ch) == -1
    }
    isNotEmpty(str){
        return str !=null
    }
}
class RegisterData{
    constructor(email, password){
        this.email = email
        this.password = password
    }
    isValid(){
        return this.email.isValid() && this.password.isValid()
    }
}

class Email extends ValidatedString{
    isValid(){
        return this.hasValidLength() && this.containsAtAndDot()
    }
    hasValidLength(){
        return DataValidator.Singleton.get().lengthIsBetween(this, 5, 20)
    }
    containsAtAndDot(){
        
    }
    domain(){
        return this.split("@")[1]
    }
}

class Password extends ValidatedString{
    isValid(){
        return this.hasValidLength() && this.deosnotContainSpace()
    }
    hasValidLength(){
        return new Validator().lengthIsBetween(this, 8, 20)
    }
    containsSpace(){
        return new Validator().doesntContainAchar(this, " ")
    }
}

class ValidatedString extends String{
    isValid(){
        return new Validator().isNotEmpty()
    }
}

function run(){
    let passwordValue = "123456789"
    let emailValue  = "me@hello.com"

    let password = new Password(passwordValue)
    let email = new Email(emailValue)
    let regData = new RegisterData(email, password)
    console.log(email.length);

    console.log(regData.isValid());
  
}

run()